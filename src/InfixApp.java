import java.util.Scanner;

/**
 * InfixApp
 */


public class InfixApp {

    public static void main(String[] args) {
        String input, output;
        while(true) {
            Scanner kb = new Scanner(System.in);
            System.out.print("Enter infix: ");
            input = kb.nextLine();
            if(input.equals("")) break;
            InToPost theTrans = new InToPost(input);
            output = theTrans.doTrans();
            System.out.println("Postfix is " + output + '\n');
        }
    }
}
